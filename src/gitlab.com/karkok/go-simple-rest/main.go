package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws"
	"strconv"
)

var dynamo *dynamodb.DynamoDB

func increment() int {
	var valStr = "value"
	var updatedNew = dynamodb.ReturnValueUpdatedNew
	updateInput := &dynamodb.UpdateItemInput{
		TableName:	aws.String("mono-increment"),
		Key:		map[string]*dynamodb.AttributeValue{
			"name": {
				S: aws.String("inc_id"),
			},
		},
		UpdateExpression: aws.String("set #value = #value + :inc"),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":inc": {
				N: aws.String(strconv.Itoa(1)),
			},
		},
		ExpressionAttributeNames: map[string]*string {
			"#value": &valStr,
		},
		ReturnValues: &updatedNew,
	}
	upValue, upErr := dynamo.UpdateItem(updateInput)
	if upErr != nil {
		panic(upErr)
	}
	newVal, strErr := strconv.Atoi(*upValue.Attributes["value"].N)
	if strErr != nil {
		panic(strErr)
	}
	return newVal
}

func createNewIdAtomicInc(c *gin.Context) {
	newVal := increment()
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "data": strconv.Itoa(newVal)})
}

func tryToIncrement() int {
	consistentReadAndWrite := true
	getInput := &dynamodb.GetItemInput{
		TableName:	aws.String("mono-increment"),
		ConsistentRead: &consistentReadAndWrite,
		Key:		map[string]*dynamodb.AttributeValue{
			"name": {
				S: aws.String("id"),
			},
		},
	}
	res, err := dynamo.GetItem(getInput)
	if err != nil {
		return -1
	}

	currentValue, convErr := strconv.Atoi(*res.Item["value"].N)
	if convErr != nil {
		return -1
	}

	var valStr = "value"
	updateInput := &dynamodb.UpdateItemInput{
		TableName:	aws.String("mono-increment"),
		Key:		map[string]*dynamodb.AttributeValue{
			"name": {
				S: aws.String("id"),
			},
		},
		UpdateExpression: aws.String("set #value = :val"),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":val": {
				N: aws.String(strconv.Itoa(currentValue + 1)),
			},
			":oldVal": {
				N: aws.String(strconv.Itoa(currentValue)),
			},
		},
		ExpressionAttributeNames: map[string]*string {
			"#value": &valStr,
		},
		ConditionExpression: aws.String("#value = :oldVal"),
	}
	_, upErr := dynamo.UpdateItem(updateInput)
	if upErr != nil {
		return -1
	}
	return currentValue + 1
}

func createNewIdConditionCheck(c *gin.Context) {
	newValue := tryToIncrement()
	j := 0
	for newValue == -1 && j < 5 {
		newValue = tryToIncrement()
		j++
	}

	if newValue == -1 {
		c.JSON(http.StatusConflict, gin.H{"status": http.StatusConflict})

	} else {
		c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "data": strconv.Itoa(newValue)})
	}
}

func init() {
	session, err := session.NewSession(&aws.Config{
		Region: aws.String("eu-west-1")}, )
	if err != nil {
		panic(err)
	}
	dynamo = dynamodb.New(session)
}

func main() {
	router := gin.Default()

	v1 := router.Group("/api/v1")
		{
			v1.POST("/mono-id-conditional", createNewIdConditionCheck)
			v1.POST("/mono-id-atomic-inc", createNewIdAtomicInc)
		}
	router.Run(":8082")
}

